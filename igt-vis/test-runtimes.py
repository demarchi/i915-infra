#!/usr/bin/env python3
#
# collect longest runtimes for tests
# data is used by shuf-tests.data
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import argparse

from visutils import *

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Exports test runtimes from piglit result json")
    ap.add_argument('files', help="Files", metavar='files', nargs='+', type=str)

    args = ap.parse_args()

    try: jsons = readfiles(args.files)
    except (KeyboardInterrupt) as e:
        print("ERROR: Keyboard interrupt while reading files")
        exit(1)

    tests = parsetests(jsons)
    builds = set([build for build,_ in sorted(jsons, key=groupbuilds)])
    hosts = set([host for _,host in sorted(jsons, key=groupbuilds)])

    print("Test,pd,ph,fd,fh,sd,sh,id,ih")
    for test in tests:
        durs = {'pass':(.0,''), 'fail':(.0,''), 'skip':(.0,'') }
        # Average over all given hosts and builds for one test
        for host in hosts:
            for build in builds:
                # care only about passed tests
                try: res = jsons[(build, host)]['tests'][test]['result']
                except: continue
                if res == 'none' or res == 'notrun' or res == 'incomplete' or res == 'abort': continue
                try:
                    dur = jsons[(build, host)]['tests'][test]['time']['end'] - jsons[(build, host)]['tests'][test]['time']['start']
                except:
                    continue
                if res=='dmesg-fail' or res=='fail' or res=='warn' or res=='dmesg-warn': res='fail'
                try:
                    if durs[res][0] > dur: continue
                    else: durs[res]=(dur,host)
                except:
                    durs[res] = (dur,host)

        print("{},{:.2f},{},{:.2f},{},{:.2f},{}".format(test,
            durs['pass'][0],durs['pass'][1],
            durs['fail'][0],durs['fail'][1],
            durs['skip'][0],durs['skip'][1]))

